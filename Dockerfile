FROM python:3.8-slim 

RUN mkdir -p /app

COPY . /app/

WORKDIR /app

RUN python -m pip install pip --upgrade && \
  python -m pip install --no-cache-dir -r /app/requirements.txt
  
ENV PYTHONPATH=/app

EXPOSE 8000

ENTRYPOINT [ "/usr/local/bin/python3.8", "/app/server.py"]

