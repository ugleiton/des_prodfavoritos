#!/usr/bin/env python
# -*- coding: utf-8 -*-


import uvicorn
from dotenv import load_dotenv

from src.utils import settings
from src import app # noqa

load_dotenv()

if __name__ == '__main__':
    app_reload = settings.get_app_debug()

    uvicorn.run(
        'server:app',
        log_level=settings.get_app_loglevel().lower(),
        host=settings.get_app_host(),
        port=settings.get_app_port(),
        reload=app_reload,
    )
