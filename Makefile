
PYTHON_BIN=venv/bin/python
COVERAGE_MIN=90

.PHONY: venv
venv:
	python3.8 -m venv venv
	$(PYTHON_BIN) -m pip install pip --upgrade
	$(PYTHON_BIN) -m pip install -r requirements/test.txt

.PHONY: run
run:
	$(PYTHON_BIN) server.py

.PHONY: gen-secret
gen-secret:
	openssl rand -hex 32

.PHONY: test
test: 
	$(PYTHON_BIN) -m pytest --cov-report xml:coverage.xml --cov-fail-under=$(COVERAGE_MIN) --cov=src -v -s

.PHONY: test-html
test-html: 
	$(PYTHON_BIN) -m pytest --cov-report html:htmlcov --cov-report xml:coverage.xml --cov-fail-under=$(COVERAGE_MIN) --cov=src -v -s

.PHONY: bandit
bandit:
	$(PYTHON_BIN) -m bandit -r -x ./venv/,./tests/ .

.PHONY: safety
safety:
	$(PYTHON_BIN) -m safety check
