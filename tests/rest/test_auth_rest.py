import pytest
from httpx import AsyncClient

from conftest import PAYLOAD_REGISTER
from src.routes import auth_v1


pytestmark = [pytest.mark.asyncio]

base_url = auth_v1.prefix


@pytest.fixture
def auto_register(client_unauthenticated: AsyncClient):
    async def _create_reg():
        res = await client_unauthenticated.post(
            f'{base_url}/register', json=PAYLOAD_REGISTER
        )
        assert res.status_code == 201
        return res.json()

    return _create_reg


async def test_regiser_with_login(client_unauthenticated, auto_register):
    new_user = await auto_register()
    assert (
        new_user['email'] == PAYLOAD_REGISTER['email']
        and new_user['name'] == PAYLOAD_REGISTER['name']
    )
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': 'application/json',
    }
    payload_data = {
        'username': PAYLOAD_REGISTER['email'],
        'password': PAYLOAD_REGISTER['password'],
    }
    res = await client_unauthenticated.post(
        base_url, data=payload_data, headers=headers
    )
    assert res.status_code == 200


async def test_duplicated_register(client_unauthenticated, auto_register):
    res = await client_unauthenticated.post(
        f'{base_url}/register', json=PAYLOAD_REGISTER
    )
    assert res.status_code == 409


async def test_invalid_register(client_unauthenticated, auto_register):
    new_payload = {
        'name': 'invalid email',
        'email': 'invalidemail',
        'password': 'secret',
    }
    res = await client_unauthenticated.post(f'{base_url}/register', json=new_payload)
    assert res.status_code == 422


async def test_incomplete_register(client_unauthenticated, auto_register):
    new_payload = {'name': 'sem passord', 'email': 'provedor@hotmail.com'}
    res = await client_unauthenticated.post(f'{base_url}/register', json=new_payload)
    assert res.status_code == 422


async def test_login_not_found(client_unauthenticated, auto_register):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': 'application/json',
    }
    payload_data = {'username': 'blabla@gmail.com', 'password': '123456789abcdefg'}
    res = await client_unauthenticated.post(
        base_url, data=payload_data, headers=headers
    )
    assert res.status_code == 404


async def test_wrong_login(client_unauthenticated, auto_register):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': 'application/json',
    }
    payload_data = {
        'username': PAYLOAD_REGISTER['email'],
        'password': '123456789abcdefg',
    }
    res = await client_unauthenticated.post(
        base_url, data=payload_data, headers=headers
    )
    assert res.status_code == 401
