import pytest

from src.routes import health_v1


pytestmark = [pytest.mark.asyncio]

base_url = health_v1.prefix


async def test_health(client):
    res = await client.get(base_url)
    assert res.status_code == 200
    return res.json()
