import pytest

from conftest import PAYLOAD_REGISTER
from src.routes import auth_v1, customer_v1


pytestmark = [pytest.mark.asyncio]

base_url = customer_v1.prefix


async def test_customer_read_me(client, payload_user):
    res = await client.get(f'{base_url}/me')
    assert res.status_code == 200
    json = res.json()
    assert json['email'] == payload_user['email']


async def test_customer_read_me_unauthenticated(client_unauthenticated):
    res = await client_unauthenticated.get(f'{base_url}/me')
    assert res.status_code == 401


async def test_customer_update_me(client, payload_user):
    path_payload = {'name': 'test update','password':'outrasenha'}
    res = await client.patch(f'{base_url}/me', json=path_payload)
    assert res.status_code == 200
    json = res.json()
    assert json['name'] != payload_user['name']


async def test_customer_update_me_invalid_field(client, payload_user):
    path_payload = {'email': 'invalidemail'}
    res = await client.patch(f'{base_url}/me', json=path_payload)
    assert res.status_code == 422


async def test_customer_update_me_duplicated(client, payload_user):
    res = await client.post(f'{auth_v1}/register', json=PAYLOAD_REGISTER)
    path_payload = {'email': PAYLOAD_REGISTER['email']}
    res2 = await client.patch(f'{base_url}/me', json=path_payload)
    assert res.status_code == 404


async def test_customer_delete_me(client):
    res = await client.delete(f'{base_url}/me')
    assert res.status_code == 204
