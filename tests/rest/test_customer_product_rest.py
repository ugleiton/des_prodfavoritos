import pytest

from conftest import PAYLOAD_MOCK_PRODUCTS, MOCK_PRODUCT_NOTFOUND
from src.routes import customer_v1


pytestmark = [pytest.mark.asyncio]

base_url = customer_v1.prefix


async def test_customer_add_product(client):
    product_id = next(iter(PAYLOAD_MOCK_PRODUCTS['products']))['id']
    paylod_new_product = {'product_id': product_id}
    res = await client.post(f'{base_url}/me/favorite-products', json=paylod_new_product)
    assert res.status_code == 201

    res2 = await client.post(
        f'{base_url}/me/favorite-products', json=paylod_new_product
    )
    assert res2.status_code == 409
    paylod_new_product = {'product_id': product_id + 'x'}
    res2 = await client.post(
        f'{base_url}/me/favorite-products', json=paylod_new_product
    )
    assert res2.status_code == 422

async def test_customer_add_product_notfound(client):
    paylod_new_product = {'product_id': MOCK_PRODUCT_NOTFOUND}
    res = await client.post(f'{base_url}/me/favorite-products', json=paylod_new_product)
    assert res.status_code == 404

async def test_customer_list_product(client):

    res = await client.get(f'{base_url}/me/favorite-products')
    assert res.status_code == 200


async def test_customer_remove_product(client):
    product_id = next(iter(PAYLOAD_MOCK_PRODUCTS['products']))['id']
    res = await client.delete(f'{base_url}/me/favorite-products/{product_id}')
    assert res.status_code == 204
