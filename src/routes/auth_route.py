from fastapi import APIRouter, Depends
from fastapi.security import OAuth2PasswordRequestForm

from src.controllers import CustomerController
from src.middlewares import authenticate_customer, create_access_token
from src.models import CustomerCreate, CustomerShow, Token
from src.utils.exceptions import BadRequestException


auth_v1 = APIRouter(
    prefix='/api/v1/auth',
    tags=['Acesso'],
)




@auth_v1.post(
    '/register',
    name='Registro para um novo acesso',
    description='Utilize essa rota para se cadastrar caso ainda não tenha acesso',
    response_model=CustomerShow,
    status_code=201,
    response_description='Cadastro criado com sucesso',
)
async def create_customer(customer: CustomerCreate):
    return await CustomerController.create_customer(customer)


@auth_v1.post('',
    name='Login',
    response_model=Token
)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    """
    Utilize essa rota obter autorização de uso nas rotas protegidas

    - Infome o **email** no campo *username*
    - Infome o **password**
    """
    customer = await authenticate_customer(form_data.username, form_data.password)
    if not customer:
        raise BadRequestException(
            detail='Incorrect email or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    access_token = create_access_token(data={'sub': customer.email})
    return {'access_token': access_token, 'token_type': 'bearer'}

