from fastapi import APIRouter

from src.controllers import HealthController


health_v1 = APIRouter(
    prefix='/api/v1/health/check',
    tags=['Saúde da aplicação'],
)


@health_v1.get('',
    name='Saúde da aplicação',
)
async def health_check():
    """
    Verifica a saúde da aplicação.
    """
    await HealthController.check()

    return {'health': 'ok'}
