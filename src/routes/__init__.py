from .auth_route import auth_v1
from .customer_route import customer_v1
from .health_route import health_v1


__all__ = ('health_v1', 'customer_v1', 'auth_v1')
