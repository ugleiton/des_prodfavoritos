from typing import List

from fastapi import APIRouter, Depends

from src.controllers import CustomerController
from src.middlewares import get_current_active_customer
from src.models import CustomerShow, CustomerUpdate, Product, ProductRef, UUIDType


customer_v1 = APIRouter(
    prefix='/api/v1/customer',
    tags=['Cliente'],
)


@customer_v1.patch(
    '/me',
    name='Atualizar próprio cadastro',
    response_model=CustomerShow,
    responses={
        404: {'description': 'Cadastro não encontrado'},
        409: {'description': 'Cadastro duplicado'},
    },
    response_description='Cadastro atualizado com sucesso',
)
async def patch_customer_me(
    customer: CustomerUpdate,
    current_customer: CustomerShow = Depends(get_current_active_customer),
):
    """
    Atualizar cadastro.
    """
    return await CustomerController.update_customer(current_customer.id, customer)


@customer_v1.get(
    '/me', 
    name='Mostrar dados do próprio cadastro',
    response_model=CustomerShow
)
async def read_customer_me(
    current_customer: CustomerShow = Depends(get_current_active_customer),
):
    """
    Mostra os dados do usuário atual.
    """
    return current_customer


@customer_v1.delete(
    '/me',
    name='Apagar próprio cadastro',
    status_code=204,
    response_description='Usuário removido com sucesso',
)
async def delete_customer_me(
    current_customer: CustomerShow = Depends(get_current_active_customer),
):
    """
    Deleta o usuário atual.
    """
    return await CustomerController.delete_customer(current_customer.id)


@customer_v1.get(
    '/me/favorite-products',
    name='Listar produtos favoritos.',
    response_model=List[Product],
)
async def list_favorite_products_me(
    current_customer: CustomerShow = Depends(get_current_active_customer),
):
    """
    Lista os produtos favoritos do usuario atual.
    """
    return await CustomerController.get_products_by_customer_id(current_customer.id)


@customer_v1.post(
    '/me/favorite-products',
    name='Adicionar produtos a lista de favoritos',
    response_model=List[Product],
    status_code=201,
    responses={
        404: {'description': 'Cadastro não encontrado'},
        409: {'description': 'Cadastro duplicado'},
    },
    response_description='Cadastro criado com sucesso',
)
async def add_favorite_products_me(
    product: ProductRef,
    current_customer: CustomerShow = Depends(get_current_active_customer),
):
    """
    Adicionar produto a a lista de favoritos.
    """
    return await CustomerController.add_product(current_customer.id, product)


@customer_v1.delete(
    '/me/favorite-products/{id}',
    name='Remover produtos da lista de favoritos',
    status_code=204,
    response_description='Produto removido com sucesso',
)
async def remove_favorite_products_me(
    id: UUIDType, current_customer: CustomerShow = Depends(get_current_active_customer)
):
    """
    Remover produto a a lista de favoritos.
    """
    return await CustomerController.pull_product(current_customer.id, id)
