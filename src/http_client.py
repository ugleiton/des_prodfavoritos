import logging
from typing import Optional

from aiohttp import ClientSession


logger = logging.getLogger(__name__)


class HttpClient:
    session: Optional[ClientSession] = None

    @classmethod
    async def startup(cls) -> None:
        if cls.session:
            logger.warning('HttpClient já foi iniciado !')
            return

        logger.info('Iniciando o HttpClient...')
        cls.session = ClientSession()

    @classmethod
    async def shutdown(cls) -> None:
        if not (session := cls.session):
            logger.warning('HttpClient já foi parado !')
            return

        logger.info('Parando o HttpClient...')
        cls.session = None
        await session.close()
