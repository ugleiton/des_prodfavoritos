from bson import ObjectId

from src.apis.product_api import ProductAPI
from src.models import Product, UUIDType
from src.utils import datetime_util, settings

from .base import Mongo, jsonable_encoder


class ProductController:
    @staticmethod
    async def get_product_local_by_id(product_id: ObjectId) -> Product:
        product = await Mongo.products.find_one({'_id': product_id})
        return Product(**product)

    @classmethod
    async def create_update_product(cls, product: Product) -> Product:
        new_product = jsonable_encoder(product)
        product_upd = await Mongo.products.find_one({'id': new_product['id']})
        new_product['updated_at'] = datetime_util.utcnow_tz()
        if product_upd is None:
            result_insert = await Mongo.products.insert_one(new_product)
            return await cls.get_product_local_by_id(result_insert.inserted_id)
        else:
            await Mongo.products.update_one(
                {'_id': product_upd['_id']}, {'$set': new_product}
            )
            return await cls.get_product_local_by_id(product_upd['_id'])

    @classmethod
    async def get_save_from_api(cls, product_id: UUIDType) -> Product:
        ## colocar circuitbreaker
        product = await ProductAPI.get_product(product_id)
        new_product = Product(**product)
        return await cls.create_update_product(new_product)

    @classmethod
    async def get_product_by_id(cls, product_id: UUIDType) -> Product:
        product = await Mongo.products.find_one({'id': product_id})
        if product is not None:
            # verificar idade do produto
            difftime = datetime_util.utcnow_tz() - product['updated_at']
            minutes = difftime.total_seconds() / 60
            #colocar cache redis
            if minutes > settings.get_cache_product_expire_minutes():
                product_upd = await cls.get_save_from_api(product_id)
                return product_upd
            else:
                return Product(**product)
        else:
            return await cls.get_save_from_api(product_id)
