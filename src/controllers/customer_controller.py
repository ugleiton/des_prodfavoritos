from typing import List

from bson import ObjectId
from pymongo.errors import DuplicateKeyError

from src.models import (
    CustomerCreate,
    CustomerShow,
    CustomerShowWithPass,
    CustomerUpdate,
    Product,
    ProductRef,
    UUIDType,
)
from src.utils import security
from src.utils.exceptions import DuplicateKeyException, NotFoundException

from .base import Mongo, jsonable_encoder
from .product_controller import ProductController


class CustomerController:
    @staticmethod
    async def get_customer_by_email(email: str) -> CustomerShowWithPass:
        customer = await Mongo.customers.find_one({'email': email})
        if customer is not None:
            return CustomerShowWithPass(**customer)
        raise NotFoundException(detail='cadastro não encontrado')

    @staticmethod
    async def get_customer_by_id(customer_id: ObjectId) -> CustomerShow:
        customer = await Mongo.customers.find_one({'_id': customer_id})
        if customer is not None:
            return CustomerShow(**customer)

        raise NotFoundException()

    @classmethod
    async def get_products_by_customer_id(cls, customer_id: ObjectId) -> List[Product]:
        customer = await Mongo.customers.find_one({'_id': customer_id})
        favorite_products = [Product(**a) for a in customer.get('favorite_products',[])]
        return favorite_products

    @classmethod
    async def create_customer(cls, customer: CustomerCreate) -> CustomerShow:
        new_customer = jsonable_encoder(customer)
        new_customer['password'] = security.get_password_hash(new_customer['password'])
        try:
            result_insert = await Mongo.customers.insert_one(new_customer)
        except DuplicateKeyError as ex:
            raise DuplicateKeyException(
                detail=f"Já exise um cadastro com os dados {ex.details['keyValue']}"
            )

        return await cls.get_customer_by_id(result_insert.inserted_id)

    @classmethod
    async def delete_customer(cls, customer_id: ObjectId) -> None:
        delete_result = await Mongo.customers.delete_one({'_id': customer_id})
        if delete_result.deleted_count != 1:
            raise NotFoundException(detail='cadastro não encontrado')

    @classmethod
    async def update_customer(
        cls, customer_id: ObjectId, customer: CustomerUpdate
    ) -> CustomerShow:
        upd_customer = customer.dict(exclude_unset=True)
        if 'password' in upd_customer:
            upd_customer['password'] = security.get_password_hash(
                upd_customer['password']
            )
        try:
            result = await Mongo.customers.update_one(
                {'_id': customer_id}, {'$set': upd_customer}
            )
        except DuplicateKeyError as ex:
            raise DuplicateKeyException(
                detail=f"Já exise um cadastro com os dados {ex.details['keyValue']}"
            )

        if result.matched_count != 1:
            raise NotFoundException(detail='Cadastro não encontrado')

        return await cls.get_customer_by_id(customer_id)

    @staticmethod
    async def check_product(customer_id: ObjectId, product_id: UUIDType) -> bool:
        """
        verificar se ja existe produto na lista.
        """
        result = await Mongo.customers.find_one(
            {'_id': customer_id, 'favorite_products.id': product_id}
        )
        if result:
            return True
        return False

    @classmethod
    async def add_product(
        cls, customer_id: ObjectId, product: ProductRef
    ) -> List[Product]:
        if await cls.check_product(customer_id, product.product_id):
            raise DuplicateKeyException(
                detail=f'O produto já se encontra na lista de favoritos'
            )
        to_add_product = await ProductController.get_product_by_id(product.product_id)
        result = await Mongo.customers.update_one(
            {'_id': customer_id},
            {'$push': {'favorite_products': to_add_product.dict()}},
        )
        if result.matched_count != 1:
            raise NotFoundException(detail='Cliente não encontrado')

        return await cls.get_products_by_customer_id(customer_id)

    @classmethod
    async def pull_product(
        cls, customer_id: ObjectId, product_id: UUIDType
    ) -> List[Product]:
        result = await Mongo.customers.update_one(
            {'_id': customer_id}, {'$pull': {'favorite_products': {'id': product_id}}}
        )
        if result.matched_count != 1:
            raise NotFoundException(detail='Cliente não encontrado')

        return await cls.get_products_by_customer_id(customer_id)
