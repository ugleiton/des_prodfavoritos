from pymongo.errors import AutoReconnect

from src.apis.product_api import ProductAPI
from src.databases import Mongo
from src.utils.exceptions import ServerException


class HealthController:
    @staticmethod
    async def check() -> None:
        try:
            await Mongo.ping()
            await ProductAPI.ping()
        except AutoReconnect as e:
            raise ServerException(detail='Falha na conexão com mongo') from e
