from fastapi import FastAPI

from src.utils import settings

from .config import configure_app


def create_application() -> FastAPI:
    app = FastAPI(
        debug=settings.get_app_debug(),
        version=settings.get_app_version(),
        title=settings.get_app_name(),
    )

    configure_app(app)

    return app


app = create_application()
