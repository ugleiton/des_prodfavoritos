from pydantic import Field, HttpUrl, PositiveFloat

from .base import BaseModel, UUIDType


class ProductRef(BaseModel):
    product_id: UUIDType = Field(...)

    class Config:
        schema_extra = {
            'example': {'product_id': '7eb953b9-927b-8bdb-6510-a06659a4aca5'}
        }


class Product(BaseModel):
    id: UUIDType = Field(...)
    price: float = Field(...)
    image: HttpUrl = Field(...)
    title: str = Field(...)
    reviewScore: PositiveFloat = Field(None)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        schema_extra = {
            'example': {
                'id': '7eb953b9-927b-8bdb-6510-a06659a4aca5',
                'title': 'Tanquinho Suggar Aleluia 4.0 4Kg',
                'image': 'http://host.exemplo.com/images/7eb953b9-927b-8bdb-6510-a06659a4aca5.jpg',
                'brand': 'suggar',
                'price': 299.0,
                'reviewScore': 4.0,
            }
        }
