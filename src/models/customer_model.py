import copy

from pydantic import Field

from .base import BaseModel, EmailType, ObjectIdStr


class Token(BaseModel):
    access_token: str
    token_type: str


class CustomerCreate(BaseModel):
    name: str = Field(...)
    email: EmailType = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            'example': {
                'name': 'Fulano de teste',
                'email': 'fulano@provedor.com',
                'password': 'secret',
            }
        }


class CustomerUpdate(CustomerCreate):
    name: str = Field(None)
    email: EmailType = Field(None)
    password: str = Field(None)


class CustomerShowWithPass(CustomerUpdate):
    id: ObjectIdStr = Field(..., alias='_id')

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True


class CustomerShow(BaseModel):
    id: ObjectIdStr = Field(..., alias='_id')
    name: str = Field(None)
    email: str = Field(...)

    class Config:
        allow_population_by_field_name = True
        arbitrary_types_allowed = True
        example_super = copy.deepcopy(CustomerUpdate.Config.schema_extra['example'])
        del example_super['password']
        schema_extra = {
            'example': {
                'id': '60678501f7d6eaade6b74e59',
                **example_super,
            }
        }
