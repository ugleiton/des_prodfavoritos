import re
import uuid

from bson import ObjectId
from pydantic import BaseModel as PydanticBaseModel


class BaseModel(PydanticBaseModel):
    ...


class ObjectIdStr(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        if isinstance(value, ObjectId):
            return value

        if not ObjectId.is_valid(value):
            raise ValueError('id inválido')
        return ObjectId(value)


class EmailType(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'
        if re.fullmatch(regex, value):
            return value

        raise ValueError('email inválido')


class UUIDType(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, value):
        # regex customizada para validar formato uuid
        regex = r'^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-z][0-9a-f]{3}-[0-9a-z][0-9a-f]{3}-[0-9a-f]{12}$'
        if not re.fullmatch(regex, value):
            raise ValueError('uuid inválido')
        try:
            uuid.UUID(value)
            return value
        except ValueError:
            raise ValueError('uuid inválido')
