import logging
from datetime import datetime, timedelta
from typing import Optional

from fastapi import Depends
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt

from src.controllers import CustomerController
from src.models import CustomerShow, CustomerShowWithPass
from src.utils import security, settings
from src.utils.exceptions import BadRequestException, UnauthorizedException


logger = logging.getLogger(__name__)
oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/api/v1/auth')


async def authenticate_customer(
    email: str,
    password: str,
) -> Optional[CustomerShow]:
    customer = await CustomerController.get_customer_by_email(email)
    if not security.verify_password(password, customer.password):
        print('password e haspassword-->', password, customer.password)
        raise UnauthorizedException(detail='não foi possível validar o password')
    return customer


def create_access_token(data: dict):
    to_encode = data.copy()
    expires_delta = timedelta(minutes=settings.get_access_token_expire_minutes())
    expire = datetime.utcnow() + expires_delta
    to_encode.update({'exp': expire})
    encoded_jwt = jwt.encode(
        to_encode, settings.get_secret_key(), algorithm=settings.get_algorithm()
    )
    return encoded_jwt


async def get_current_customer(token: str = Depends(oauth2_scheme)):
    credentials_exception = UnauthorizedException(
        detail='não foi possível validar credenciais',
        headers={'WWW-Authenticate': 'Bearer'},
    )
    try:
        payload = jwt.decode(
            token, settings.get_secret_key(), algorithms=[settings.get_algorithm()]
        )
        email: str = payload.get('sub')
        if email is None:
            raise credentials_exception
        customer = await CustomerController.get_customer_by_email(email)
    except JWTError:
        raise credentials_exception
    # if user is None:
    #     raise credentials_exception
    return customer


async def get_current_active_customer(
    current_customer: CustomerShowWithPass = Depends(get_current_customer),
):
    return current_customer
