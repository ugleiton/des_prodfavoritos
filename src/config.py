from bson import ObjectId
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

# from des_prodfavorito.middlewares import generate_superuser
from src.apis.product_api import ProductAPI
from src.databases import MigrationManager, Mongo
from src.utils import pydantic
# from fastapi.staticfiles import StaticFiles
from src.utils.logging import start_logging, stop_logging

from .http_client import HttpClient
from .routes import auth_v1, customer_v1, health_v1


def configure_middlewares(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=['*'],
        allow_credentials=True,
        allow_methods=['GET', 'POST', 'PUT', 'DELETE'],
        allow_headers=['*'],
    )


def configure_routes(app: FastAPI):
    # app.include_router(gitlab_mock)
    app.include_router(health_v1)
    app.include_router(auth_v1)
    app.include_router(customer_v1)


def configure_app(app: FastAPI):
    start_logging()

    # configure_exception_handlers(app)
    configure_middlewares(app)
    configure_routes(app)

    app.on_event('startup')(startup_event)
    app.on_event('shutdown')(shutdown_event)

    # configure_static_files(app)


async def startup_event():
    await HttpClient.startup()
    # conversão de ObjectId para str
    pydantic.register_encoder(ObjectId, str)
    Mongo.start()
    await MigrationManager.migrate()
    ProductAPI.start()


async def shutdown_event():
    await HttpClient.shutdown()
    Mongo.stop()
    await stop_logging()
