import logging
from typing import Optional

from motor.core import AgnosticClient, AgnosticCollection, AgnosticDatabase
from motor.motor_asyncio import AsyncIOMotorClient

from src.utils import settings


logger = logging.getLogger(__name__)


class Mongo:

    client: Optional[AgnosticClient] = None
    db: Optional[AgnosticDatabase] = None

    # collections
    customers: Optional[AgnosticCollection] = None
    products: Optional[AgnosticCollection] = None
    migrations: Optional[AgnosticCollection] = None

    @classmethod
    async def ping(cls) -> None:
        await cls.client.admin.command({'ping': 1})

    @classmethod
    def start(cls) -> None:
        if cls.client is not None:
            # raise RuntimeError("Conexão com mongo já foi iniciada")
            logger.warning('Conexão com mongo já foi iniciada')
            return
        logger.info('Iniciando conexão com mongo')

        cls.config_database(settings.get_mongodb_url())

    @classmethod
    def config_database(cls, database_url: str = None) -> None:
        cls.client = AsyncIOMotorClient(database_url, tz_aware=True)
        cls.db = cls.client.get_default_database()
        # instanciando collections
        cls.config_collections()

    @classmethod
    def config_collections(
        cls,
    ) -> None:
        cls.customers = cls.db['customers']
        cls.products = cls.db['products']
        cls.migrations = cls.db['migrations']

    @classmethod
    def stop(cls) -> None:
        if cls.client is None:
            raise RuntimeError('Conexão com mongo já foi desligado')
