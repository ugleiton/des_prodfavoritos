import asyncio
import logging
import os
import re
import sys

from src.utils import datetime_util

from .mongo import Mongo


logger = logging.getLogger(__name__)
from motor.core import AgnosticDatabase


class MigrationManager:
    MIGRATIONS_COLLECTION = 'migrations'
    MIGRATIONS_PATH = 'migrations'

    @staticmethod
    def _valid_file_migration(file_migration):
        regex = r'^[0-9]{11}[_|-][a-zA-Z0-9-_]*.py$'
        if re.fullmatch(regex, file_migration):
            return True
        return False

    @classmethod
    async def _get_migrations_mongo(cls, db):
        cursor = db[cls.MIGRATIONS_COLLECTION].find({}, {'name': 1})
        return [doc['name'] async for doc in cursor]

    @classmethod
    async def _save_migration_mongo(
        cls, db: AgnosticDatabase, migration_name: str
    ) -> None:
        await db[cls.MIGRATIONS_COLLECTION].insert_one(
            {'name': migration_name, 'created_at': datetime_util.utcnow_tz()}
        )

    @classmethod
    async def migrate(cls) -> None:
        logger.info('Verificando migrations com mongo')
        db = Mongo.db
        # adicionando diretorio de migrations ao path para importação abaixo
        sys.path.insert(0, cls.MIGRATIONS_PATH)
        database_migrations = await cls._get_migrations_mongo(db)
        for filename in os.listdir(cls.MIGRATIONS_PATH):
            filemodule = filename[:-3]

            """verifica se está de acordo com pattern definido ou ja foi migrado"""
            if (
                not cls._valid_file_migration(filename)
                or filemodule in database_migrations
            ):
                continue

            try:
                module = __import__(filemodule)
                upgrade = getattr(module, 'up', None)
                if not upgrade:
                    logger.warning('Não existe método `up` na migração %s', filemodule)
                    continue

                if not asyncio.iscoroutinefunction(upgrade):
                    raise RuntimeError('O método `upgrade` deve ser asincrono')
                logger.info(f'Aplicando migration {filemodule}')
                await upgrade(db)
                await cls._save_migration_mongo(db, filemodule)

            except Exception as e:
                logger.exception('Falha aplicando migração: %s', filemodule, exc_info=e)
                raise
