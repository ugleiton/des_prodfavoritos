import logging
from typing import Dict

import aiohttp

from src.models import UUIDType
from src.utils import settings
from src.utils.exceptions import NotFoundException,BadRequestException


logger = logging.getLogger(__name__)


class ProductAPI:
    base_url = None

    @classmethod
    def get_url(cls, path):
        slash = ''
        if not cls.base_url.endswith('/'):
            slash = '/'
        return f'{cls.base_url}{slash}{path}'

    @classmethod
    def start(cls) -> None:
        logger.info('Iniciando configuração da api de produtos')
        if cls.base_url is None:
            cls.base_url = settings.get_products_base_url()

    @classmethod
    async def ping(cls) -> None:
        async with aiohttp.ClientSession() as session:
            async with session.get(cls.get_url('product/?page=1')) as resp:
                result_status = resp.status
                if result_status != 200:
                    raise BadRequestException(detail=f'api de produtos nao está disponivel {cls.base_url}')
                logger.debug(
                    f'Conexão de teste api de produtos, status={result_status}'
                )
                return await resp.json()

    @classmethod
    async def get_product(cls, product_id: UUIDType) -> Dict:
        async with aiohttp.ClientSession() as session:
            async with session.get(cls.get_url(f'product/{product_id}/')) as resp:
                if resp.status == 404:
                    raise NotFoundException(detail=f'produto não encontrado na api {cls.base_url}')
                return await resp.json()
