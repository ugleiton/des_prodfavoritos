from typing import Any, Callable, Type

import pydantic


def register_encoder(object_type: Type[Any], encoder: Callable[[Any], Any]):
    pydantic.json.ENCODERS_BY_TYPE[object_type] = encoder
