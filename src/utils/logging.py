import logging
from logging import Logger, getLogger


def get_logger(name: str) -> Logger:
    return getLogger(name)


def start_logging() -> None:
    logging.basicConfig(
        format='%(asctime)s: [%(levelname)s] - %(message)s',
        level=logging.DEBUG,
    )


async def stop_logging() -> None:
    ...


logger = get_logger(__name__)
