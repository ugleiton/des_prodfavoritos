from datetime import datetime, timezone


def utcnow_tz() -> datetime:
    return datetime.utcnow().replace(tzinfo=timezone.utc)
