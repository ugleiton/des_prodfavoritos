from distutils.util import strtobool
from os import getenv


def _get_env_bool(key: str, default: str) -> bool:
    return bool(strtobool(getenv(key, default)))


def get_app_debug() -> bool:
    return _get_env_bool('APP_DEBUG', 'False')


def get_app_name() -> str:
    return 'Produtos Favoritos'


def get_app_version() -> str:
    return '0.0.1'


def get_app_loglevel() -> str:
    return getenv('LOG_LEVEL', 'debug')


def get_app_port() -> str:
    return int(getenv('APP_PORT', 8000))


def get_app_host() -> str:
    return getenv('APP_HOST', '127.0.0.1')


def get_mongodb_url() -> str:
    return getenv('MONGO_URL', 'mongodb://localhost:27017/develop')


def get_mongodb_url_test() -> str:
    return getenv('MONGO_URL_TEST', 'mongodb://localhost:27017/develop')


def get_products_base_url() -> str:
    return getenv('PRODUCTS_BASE_URL', 'http://localhost.produtos/api/')


def get_access_token_expire_minutes() -> int:
    # 1440 = 24*60=1 dia
    return int(getenv('ACCESS_TOKEN_EXPIRE_MINUTES', 1440))


def get_cache_product_expire_minutes() -> int:
    return int(getenv('CACHE_PRODUCT_EXPIRE_MINUTES', 60))


def get_secret_key() -> int:
    return getenv(
        'SECRET_KEY', '13164469bcad0e46587f7c66e955c96a84774d65839eab8f31f08287878e4d3a'
    )


def get_algorithm() -> int:
    return 'HS256'
