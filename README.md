# Desafio Produtos Favoritos

Esse desafio é uma api em python para gerenciar os favoritos dos clientes autenticados.

## Pré-requisitos

- Python 3.8
- Mongo 4.2

## Configuração

Configure as variáveis de ambiente informadas abaixo, criando um arquivo .env na raiz do projeto.

| Variável                    | Obrigatório | Descrição                                                                            |
|-----------------------------|:------------|:-------------------------------------------------------------------------------------|
| APP_HOST                    |             | Rede disponibilizar o serviço, padrão 127.0.0.1, utilize 0.0.0.0 para a rede local.  |
| APP_PORT                    |             | Porta para expor o serviço. padrão 8000                                              |
| SECRET_KEY                  |    Sim      | Chave para encriptar token jwt, gere uma nova com o comando **openssl rand -hex 32** |
| MONGO_URL                   |    Sim      | Uri para acesso ao banco de producao do mongo                                        |
| MONGO_URL_TEST              |             | Uri para acesso ao banco de teste do mongo, **obrigatorio para testes**              |
| ACCESS_TOKEN_EXPIRE_MINUTES |             | Tempo de expiração padrão dos tokens de acesso dos clientes                          |
| PRODUCTS_BASE_URL           |    Sim      | API para acesso aos produtos do desafio, formato http://host/api/                    |

## Instalação e inicialização

Utilize o gerenciador de pacotes pip para instalar as dependências.

- Ambiente produção

```bash
pip install -r requirements.txt
python server.py
```

- Testes com pytest

```bash
pip install -r requirements/test.txt
pytest --cov-fail-under=90 --cov=src -v -s
```

## Documentação da API

Ao iniciar o serviço acesse um dos endereços abaixo para obter a documentação padrão openapi, no endereço altere *localhost:8000* para *host-ou-ip:porta* caso tenha iniciado o serviço em algum servidor. 

 - Swagger: **http://localhost:8000/docs**
 - Redoc: **http://localhost:8000/redoc**
 - Json OpenApi: **http://localhost:8000/openapi.json**


### Resumo descritivo das rotas, a documentação completa pode ser acessada nos endereços acima.

#### GET /api/v1/health/check

##### Descrição
Utilizada para identificar se a aplicação está ativa conectada com mongo e api de produtos.

##### Exemplo

```sh
curl -X 'GET' \
  'http://localhost:8000/api/v1/health/check' \
  -H 'accept: application/json'
```

#### POST /api/v1/auth/register

##### Descrição
Utilizada para o cliente se cadastrar caso ainda não tenha acesso a api.

##### Exemplo

```sh
curl -X 'POST' \
  'http://localhost:8000/api/v1/auth/register' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Fulano de teste",
  "email": "fulano@provedor.com",
  "password": "secret"
}'
```

#### POST /api/v1/auth

##### Descrição
Utilizada para o cliente obter o token jwt, esse token é necessário para as próximas rotas.

##### Exemplo

```sh
curl -X 'POST' \
  'http://localhost:8000/api/v1/auth' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'username=fulano%40provedor.com&password=secret'
```

#### GET /v1/customer/me

##### Descrição
Rota utilizada devolver as informações cadastrados do cliente logado.

##### Exemplo

```sh
curl -X 'GET' \
  'http://localhost:8000/api/v1/customer/me' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M'
```

#### PATCH /api/v1/customer/me

##### Descrição
Utilizada para atualizar o cadastro do cliente logado.

##### Exemplo

```sh
curl -X 'PATCH' \
  'http://localhost:8000/api/v1/customer/me' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Fulano de teste2",
  "password": "secret"
}'
```


#### DELETE /api/v1/customer/me

##### Descrição
Para que o cliente consiga apagar o proprio cadastro.

##### Exemplo

```sh
curl -X 'DELETE' \
  'http://localhost:8000/api/v1/customer/me' \
  -H 'accept: */*' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M'
```

#### GET /v1/customer/me/favorite-products

##### Descrição
Utilizada para listar os produtos favoritos salvos pelo cliente.

##### Exemplo

```sh
curl -X 'GET' \
  'http://localhost:8000/api/v1/customer/me/favorite-products' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M'
```

#### POST /v1/customer/me/favorite-products

##### Descrição
Adicionar um novo produto a lista de favoritos do cliente logado.

##### Exemplo

```sh
curl -X 'POST' \
  'http://localhost:8000/api/v1/customer/me/favorite-products' \
  -H 'accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M' \
  -H 'Content-Type: application/json' \
  -d '{
  "product_id": "7eb953b9-927b-8bdb-6510-a06659a4aca5"
}'
```

#### DELETE /api/v1/customer/me/favorite-products/{id-produto}

##### Descrição
Remover um produto da lista de favoritos do cliente logado.

##### Exemplo

```sh
curl -X 'DELETE' \
  'http://localhost:8000/api/v1/customer/me/favorite-products/7eb953b9-927b-8bdb-6510-a06659a4aca5' \
  -H 'accept: */*' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ1Z2xlaXRvbkBnbWFpbC5jb20iLCJleHAiOjE2NDM3NDU2MDN9.CMc95qr2vq0OqQ9M-iDGVo0hqlua5Gr59yzWpsx-T3M'
```

## Licença
[MIT](https://gitlab.com/ugleiton/des_prodfavoritos/-/blob/60a8d2a4312192a1f8b37216a97b5260f38c5c4e/LICENSE)
