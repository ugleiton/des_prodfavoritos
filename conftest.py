import asyncio
import json
import logging

import pytest
from aioresponses import aioresponses
from asgi_lifespan import LifespanManager
from dotenv import load_dotenv
from httpx import AsyncClient


load_dotenv()
logger = logging.getLogger(__name__)

from src.main import create_application
from src.utils import settings


@pytest.fixture
def payload_user():
    return {'name': 'test', 'email': 'fulano@test.com', 'password': 'secret'}


PAYLOAD_REGISTER = {
    'name': 'Fulano de teste',
    'email': 'fulano@provedor.com',
    'password': 'secret',
}

PAYLOAD_MOCK_PRODUCTS = {
    'meta': {'page_number': 1, 'page_size': 100},
    'products': [
        {
            'price': 1699.0,
            'image': 'http://challenge-api.luizalabs.com/images/1bf0f365-fbdd-4e21-9786-da459d78dd1f.jpg',
            'brand': 'bébé confort',
            'id': '1bf0f365-fbdd-4e21-9786-da459d78dd1f',
            'title': 'Cadeira para Auto Iseos Bébé Confort Earth Brown',
        }
    ],
}

MOCK_PRODUCT_NOTFOUND = "f50fb7d6-6398-417c-b2e9-009cdda1158a"

def mock_products(responses):
    """
    mock para products usando aioresponses.
    """
    from src.utils.settings import get_products_base_url

    base_url_produtos = get_products_base_url()

    url = f'{base_url_produtos}product/?page=1'
    logger.debug(f'Gerando mock para {url}')
    responses.get(url, body=json.dumps(PAYLOAD_MOCK_PRODUCTS), status=200)

    body_product = next(iter(PAYLOAD_MOCK_PRODUCTS['products']))
    product_id = body_product['id']
    url = f'{base_url_produtos}product/{product_id}/'
    logger.debug(f'Gerando mock para {url}')
    responses.get(url, body=json.dumps(body_product), status=200)

    product_id = MOCK_PRODUCT_NOTFOUND
    url = f'{base_url_produtos}product/{product_id}/'
    logger.debug(f'Gerando mock para {url}')
    responses.get(url, status=404)


@pytest.fixture(scope='session')
async def test_app_with_db():
    from src.databases import Mongo

    # configurando mongo para base de testes
    Mongo.config_database(settings.get_mongodb_url_test())
    app = create_application()
    with aioresponses() as responses:
        mock_products(responses)
        async with LifespanManager(app):
            yield app


@pytest.fixture(scope='function')
async def wipe_mongo(event_loop):
    from src.databases import Mongo

    await Mongo.customers.drop()
    await Mongo.products.drop()
    await Mongo.migrations.drop()
    return


@pytest.fixture(scope='function')
async def header_token(event_loop, wipe_mongo, payload_user):
    from src.controllers import CustomerController
    from src.middlewares import create_access_token
    from src.models import CustomerCreate

    # user = event_loop.run_until_complete(CustomerController.create_customer(CustomerCreate(**new_user)))
    user = await CustomerController.create_customer(CustomerCreate(**payload_user))
    access_token = create_access_token(data={'sub': user.email})
    return {'Authorization': f'Bearer {access_token}'}


@pytest.fixture
async def client(test_app_with_db, header_token):
    async with AsyncClient(
        app=test_app_with_db, base_url='http://test', headers=header_token
    ) as client:
        yield client


@pytest.fixture
async def client_unauthenticated(test_app_with_db):
    async with AsyncClient(app=test_app_with_db, base_url='http://test') as client:
        yield client


@pytest.fixture(scope='session')
def event_loop():
    return asyncio.get_event_loop()
